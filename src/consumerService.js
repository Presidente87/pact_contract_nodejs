const { server } = require("./__tests__/consumer")

server.listen(8080, () => {
  console.log("Frontend running on http://localhost:8080")
})