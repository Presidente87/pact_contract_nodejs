const path = require ("path")
const {Verifier} = require ("@pact-foundation/pact")
const {server, importData} = require ("../../../src/provider")
const SERVER_URL = "http://localhost:8081"

server.listen(8081, () => {
    importData()

})

